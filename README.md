![Build Status](https://gitlab.com/danarbaugh/danarbaugh.gitlab.io/badges/master/build.svg) | [![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)

---

Dan Arbaugh's Resume via [Gatsby](https://github.com/gatsbyjs/gatsby) on GitLab Pages.

---

This is the result of a Jekyll/Angular dude trying out Gatsby/React.

----

Forked from https://github.com/gatsbyjs/gatsby-starter-default
