import React, { Fragment } from 'react';
import Helmet from 'react-helmet';

import Header from '../components/header';

import './index.scss';

const Body = () => (
  <article>
    <h5>Experience</h5>
    <div className="job">
      <div className="title">Software Engineer</div>
      <div className="timespan">Dec 2018 - Present</div>
      <div className="employer">O&apos;Reilly Media</div>
      <div className="location">Sebastopol, CA (remote)</div>
    </div>
    <div className="job">
      <div className="title">Software Engineer</div>
      <div className="timespan">Apr 2017 - Dec 2018</div>
      <div className="employer">Ciholas, Inc.</div>
      <div className="location">Newburgh, IN</div>
    </div>
    <div className="job">
      <div className="title">Programmer Analyst Sr</div>
      <div className="timespan">Sep 2014 - Apr 2017</div>
      <div className="employer">OneMain Financial Inc.</div>
      <div className="location">Evansville, IN</div>
    </div>
    <div className="job">
      <div className="title">Web Developer</div>
      <div className="timespan">May 2013 - Sep 2014</div>
      <div className="employer">Lieberman Technologies</div>
      <div className="location">Evansville, IN</div>
    </div>
    <h5>Skills</h5>
    <div className="skills">
      <div className="grid">
        <div className="col">
          <ul>
            <li><strong>Languages</strong></li>
            <ul>
              <li>Javascript</li>
              <ul>
                <li>Angular (1-6)</li>
                <li>React</li>
                <li>Node.js</li>
              </ul>
              <li>Python</li>
              <li>Java</li>
              <li>C/C++</li>
              <li>PHP</li>
              <li>Go</li>
            </ul>
            <li><strong>Continuous Integration</strong></li>
            <ul>
              <li>GitLab CI</li>
              <li>Jenkins</li>
            </ul>
          </ul>
        </div>
        <div className="col">
          <ul>
            <li><strong>Mobile Development</strong></li>
            <ul>
              <li>Native Android</li>
              <li>Native iOS</li>
              <li>Hybrid (Cordova, React Native)</li>
            </ul>
            <li><strong>Web Development</strong></li>
            <ul>
              <li>HTML/CSS</li>
              <li>WebSocket</li>
              <li>Apache/Nginx</li>
            </ul>
            <li><strong>Server Administration</strong></li>
            <ul>
              <li>RHEL/Debian-based distros</li>
              <li>AWS/Google Cloud Platform</li>
              <li>Serverless architectures</li>
              <li>Docker</li>
            </ul>
          </ul>
        </div>
      </div>
    </div>
    <h5>Education</h5>
    <div className="education">
      <div className="major">Bachelor of Science, Computer Science</div>
      <div className="timespan">2010 - 2014</div>
      <div className="college">University of Southern Indiana</div>
      <div className="location">Evansville, IN</div>
    </div>
  </article>
);

const IndexPage = () => (
  <div className="mdl-layout mdl-layout--fixed-header mdl-js-layout">
    <Fragment>
      <div id="page" className="mdl-layout__content mdl-color--white content mdl-color-text--grey-800">
        <Helmet
          title="Dan Arbaugh | dan@danarbaugh.com"
          meta={[
            { name: 'description', content: 'Dan Arbaugh Resume - Developer for Hire' },
            { name: 'keywords', 
              content: `software, engineer, javascript, node, angular, react, java, python, docker, html, css,
                evansville, indiana, united states, remote, consulting, frontend, backend, full-stack, developer`,
            },
          ]}
          link={[
            {
              rel: 'stylesheet',
              href: 'https://fonts.googleapis.com/icon?family=Material+Icons|Roboto:regular,bold,italic',
            },
          ]}
        />
        <Header />
        <Body />
      </div>
    </Fragment>
  </div>
);

export default IndexPage;
