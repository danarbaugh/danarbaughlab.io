import React from 'react';

const NotFoundPage = () => (
  <div>
    <h1>404 DAN NOT FOUND</h1>
    <p>Where is he?!</p>
  </div>
);

export default NotFoundPage;
