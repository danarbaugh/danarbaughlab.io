import React from 'react';
import Link from 'gatsby-link';

const Header = () => (
  <header>
    <h4>
      <Link to="/">
        Dan Arbaugh
      </Link>
    </h4>
    <div>
      Newburgh, IN
    </div>
    <div>
      dan@danarbaugh.com
    </div>
  </header>
);

export default Header;
